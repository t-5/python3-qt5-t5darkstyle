#!/bin/bash

cd ..
DT=`date +%Y%m%d_%H%M%S `
aptly snapshot create t-5_${DT} from repo t-5
aptly publish drop repo
aptly -distribution="repo" publish snapshot t-5_${DT}
/usr/local/jh/bin/aptly_purgeold.py t-5 python3-qt5-t5darkstyle 2
rsync -r --progress --delete /home/jh/.aptly/public/ /home/jh/t-5.eu/debian-repo/

twine upload --repository-url https://upload.pypi.org/legacy/ dist/*
